#!/usr/bin/env python
import jsons
from jwt import decode
from flask import Flask, escape, request, Blueprint, jsonify, make_response, jsonify, Response
from flask_jwt import JWT, jwt_required, current_identity
from sigaa_api.rbac.infraestructura.AuthWS import AuthWS
from sigaa_api.rbac.infraestructura.RbacPGSQL import RbacPGSQL
from sigaa_api.shared.infraestructura.Logger import logger

rbac_blueprint = Blueprint('rbac', __name__)

class RbacWS(object):
    """
    Define el blueprint para flask
    """    
    def __init__(self):
        self.dbRepo = RbacPGSQL()
        self.auth = AuthWS()
    @staticmethod
    def response_json(info="RbacWS",data={},code=200):
        """
        Plantilla para las respuestas RbacWS
        """
        response = make_response(jsons.dumps({
            "info":info,
            "code":code,
            "data":data
        }), code)
        response.headers["Content-Type"] = "application/json"
        return response

    @staticmethod
    def get_rbac_jwt():
        jwt_token = request.headers["JWTAuthorization"]
        encodedJWT = jwt_token.split(" ")[1]
        jwt_dict = decode(encodedJWT,"super-secret",algorithms=['HS256'])
        return jwt_dict["rbac"]


@rbac_blueprint.route('/ws/sigaa_api/rbac/perfiles',methods=['GET'])
def get_perfiles():
    """
    Obtener todos los perfiles
    ---
    tags:
        - rbac
        - perfil
    responses:
        200:
            description: Devuelve todos los perfiles existentes
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    perfiles = rbacWS.dbRepo.get_perfiles()
    try:
        return RbacWS.response_json(info="Obtener perfiles",data=perfiles)
    except Exception as e:
        return RbacWS.response_json(info="Obtener perfiles - error",code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/perfil/<perfil_id>',methods=['GET'])
def get_perfil(perfil_id):
    """
    Obtener perfil
    ---
    tags:
        - rbac
        - perfil
    parameters:
        - in: path
          name: perfil_id
          type: string
          required: true
          description: uuid del perfil
    responses:
        200:
            description: Devuelve el perfil
        400:
            description: No se encontro el perfil en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        perfil = rbacWS.dbRepo.get_perfil(perfil_id)
        return RbacWS.response_json(info="Obtener perfil",data=perfil)
    except TypeError:
        return RbacWS.response_json(info="Obtener perfil - no encontrado",data={}, code=400)
    except:
        return RbacWS.response_json(info="Obtener perfil - Error",data={}, code=500)
    

@rbac_blueprint.route('/ws/sigaa_api/rbac/perfil/<perfil_id>',methods=['PUT'])
@jwt_required()
def update_perfil(perfil_id):
    """
    Actualizar perfil
    ---
    tags:
        - rbac
        - perfil
    consumes:
        - application/json
    parameters:
        - in: path
          name: perfil_id
          type: string
          required: true
          description: uuid del perfil
        - name: body
          in: body
          required: true
          schema:
            required:
                - info
            properties:
                info:
                    type: object
                    description: la informacion del perfil
            example:
                info: {"password":"contraseña123","fecha_activo":{"inicio":"2020-10-10T09:00:00.0Z","fin":"2020-10-11T21:00:00.0Z"}}
    responses:
        200:
            description: Devuelve el perfil
        400:
            description: No se encontro el perfil en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    rbac = RbacWS.get_rbac_jwt()
    try:
        data = request.get_json()
        perfil = rbacWS.dbRepo.get_perfil(perfil_id)
        perfil_new = rbacWS.dbRepo.update_perfil(data)
        return RbacWS.response_json(info="Actualizar perfil", data=perfil_new)
    except Exception as e:
        return RbacWS.response_json(info="Actualizar perfil - Error en la solicitud", 
            data=e, code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/perfil',methods=['POST'])
def crear_perfil():
    """
    Crear perfil
    ---
    tags:
        - rbac
        - perfil
    consumes:
        - application/json
    parameters:
        - name: body
          in: body
          required: true
          schema:
            required:
                - info
            properties:
                info:
                    type: object
                    description: la informacion del perfil
    responses:
        200:
            description: Crea el perfil con exito
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    
    perfil = rbacWS.dbRepo.crear_perfil()
    data = request.get_json()
    if data["info"]:
        perfil.set_info(data["info"])
        perfil = rbacWS.dbRepo.update_perfil(perfil.to_json())
    return RbacWS.response_json(info="Crear perfil - Creado con exito",data=perfil, code=201)

@rbac_blueprint.route('/ws/sigaa_api/rbac/perfil/<perfil_id>',methods=['DELETE'])
def delete_perfil(perfil_id):
    """
    Eliminar perfil
    ---
    tags:
        - rbac
        - perfil
    parameters:
        - in: path
          name: perfil_id
          type: string
          required: true
          description: uuid del perfil
    responses:
        200: 
            description: Perfil eliminado
        400:
            description: No se encontro el perfil en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        a = rbacWS.dbRepo.delete_perfil(perfil_id)
        return RbacWS.response_json(info="Eliminar perfil - Exito")
    except Exception as e:
        return RbacWS.response_json(info="Eliminar perfil - Error", data=e, code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/roles',methods=['GET'])

def get_roles():
    """
    Obtener roles
    ---
    tags:
        - rbac
        - rol
    responses:
        200: 
            description: Roles devueltos con exito
        400:
            description: No se encontraron roles en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    roles_dict = []
    try:
        roles = rbacWS.dbRepo.get_roles()
        for rol in roles:
            roles_dict.append(rol.to_dict())
        return RbacWS.response_json(info="Obtener roles", data=roles_dict)
    except Exception as e:
        return RbacWS.response_json(info="Obtener roles - Error", data=e, code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/rol/<rol_id>',methods=['GET'])
def get_rol(rol_id):
    """
    Obtener rol
    ---
    tags:
        - rbac
        - rol
    parameters:
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
    responses:
        200: 
            description: Rol devuelto con exito
        400:
            description: No se encontro el rol en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        rol = rbacWS.dbRepo.get_rol(rol_id).to_dict()
        return RbacWS.response_json(info="Obtener rol", data=rol)
    except TypeError:
        return RbacWS.response_json(info="Obtener rol - No encontrado", data={}, code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/rol/<rol_id>',methods=['PUT'])
def update_rol(rol_id):
    """
    Actualizar rol
    ---
    tags:
        - rbac
        - rol
    consumes:
        - application/json
    parameters:
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
        - in: body
          required: true
          schema:
            required:
                - nombre
                - configuracion
            properties:
                nombre:
                    type: string
                    description: Nombre del rol
                configuracion:
                    type: object
                    description: Configuracion del rol
            example:
                nombre: "administrador supremo"
                configuracion: {fecha_activo:{"inicio":"2020-10-10T09:00:00.0Z","fin":"2020-10-11T21:00:00.0Z"}}
    responses:
        200:
            description: Rol actualizado con exito
        400:
            description: No se encontro el rol en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rol = request.get_json()
    rol["id"] = rol_id
    rol["permisos"] = []
    rol["hijos"] = []
    rbacWS = RbacWS()
    try:
        rol = rbacWS.dbRepo.update_rol(jsons.dumps(rol))
        return RbacWS.response_json(info="Actualizar rol", data=rol)
    except Exception as e:
        return RbacWS.response_json(info="Actualizar rol - Error", data=e, code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/rol/<rol_id>',methods=['DELETE'])
def delete_rol(rol_id):
    """
    Eliminar rol
    ---
    tags:
        - rbac
        - rol
    parameters:
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
    responses:
        200: 
            description: Rol eliminado
        400:
            description: No se encontro el rol en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        rol = rbacWS.dbRepo.get_rol(rol_id)
        rbacWS.dbRepo.delete_rol(rol_id)
        return RbacWS.response_json(info="Eliminar rol", data=rol)
    except Exception as e:
        return RbacWS.response_json(info="Eliminar rol - Error", data=e, code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/rol/<nombre>',methods=['POST'])
def crear_rol(nombre):
    """
    Crear rol
    ---
    tags:
        - rbac
        - rol
    parameters:
        - in: path
          name: nombre
          type: string
          required: true
          description: nombre del rol
    responses:
        200: 
            description: Rol Creado
        400:
            description: Error al crear el rol, dato de nombre no valido
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        rol = rbacWS.dbRepo.crear_rol(nombre)
        return RbacWS.response_json(info="Crear rol - Creado con exito",data=rol, code=201)
    except Exception as e:
        logger.error("{}".format(e))
        return RbacWS.response_json(info="Crear rol - Error al crear rol", code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/permiso/<nombre>',methods=['POST'])
def crear_permiso(nombre):
    """
    Crear permiso
    ---
    tags:
        - rbac
        - permiso
    parameters:
        - in: path
          name: nombre
          type: string
          required: true
          description: nombre del permiso
    responses:
        200: 
            description: Permiso Creado
        400:
            description: Error al crear el permiso, dato de nombre no valido
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        permiso = rbacWS.dbRepo.crear_permiso(nombre)
        return RbacWS.response_json(info="Crear permiso - Creado con exito",data=permiso, code=201)
    except Exception as e:
        logger.error("{}".format(e))
        return RbacWS.response_json(info="Crear permiso - Error al crear permiso", code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/permiso/<permiso_id>',methods=['GET'])
def get_permiso(permiso_id):
    """
    Obtener permiso
    ---
    tags:
        - rbac
        - permiso
    parameters:
        - in: path
          name: permiso_id
          type: string
          required: true
          description: uuid del permiso
    responses:
        200: 
            description: Permiso devuelto con exito
        400:
            description: Permiso no encontrado en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        permiso = rbacWS.dbRepo.get_permiso(permiso_id).to_dict()
        return RbacWS.response_json(info="Obtener permiso",data=permiso)
    except Exception as e:
        logger.error("{}".format(e))
        return RbacWS.response_json(info="Obtener permiso - Error al obtener permiso", code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/permisos',methods=['GET'])
def get_permisos():
    """
    Obtener permisos
    ---
    tags:
        - rbac
        - permiso
    responses:
        200: 
            description: Permisos devueltos con exito
        400:
            description: No se encontraron permisos en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        permisos_dict = []
        permisos = rbacWS.dbRepo.get_permisos()
        for permiso in permisos:
            permisos_dict.append(permiso.to_dict())
        return RbacWS.response_json(info="Obtener permisos", data=permisos_dict)
    except:
        return RbacWS.response_json(info="Obtener permisos - Error al obtener permisos", code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/permiso/<permiso_id>',methods=['PUT'])
def update_permiso(permiso_id):
    """
    Actualizar permiso
    ---
    tags:
        - rbac
        - permiso
    consumes:
        - application/json
    parameters:
        - in: path
          name: permiso_id
          type: string
          required: true
          description: uuid del permiso
        - in: body
          required: true
          schema:
            required:
                - nombre
                - configuracion
            properties:
                nombre:
                    type: string
                    description: Nombre del permiso
                configuracion:
                    type: object
                    description: Configuracion del permiso
            example:
                nombre: "reportes de calificaciones"
                configuracion: {fecha_activo:{"inicio":"2020-10-10T09:00:00.0Z","fin":"2020-10-11T21:00:00.0Z"}}
    responses:
        200:
            description: Permiso actualizado con exito
        400:
            description: No se encontro el permiso en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    permiso = request.get_json()
    permiso["id"] = permiso_id
    permiso["hijos"] = []
    rbacWS = RbacWS()
    try:
        rol = rbacWS.dbRepo.update_permiso(jsons.dumps(permiso))
        return RbacWS.response_json(info="Actualizar rol - Exito", data=rol)
    except Exception as e:
        return RbacWS.response_json(info="Actualizar rol - Error", data=e, code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/permiso/<permiso_id>',methods=['DELETE'])
def eliminar_permiso(permiso_id):
    """
    Eliminar un permiso
    ---
    tags:
        - rbac
        - permiso
    parameters:
        - in: path
          name: permiso_id
          type: string
          required: true
          description: uuid del permiso
    responses:
        200:
            description: Permiso eliminado con exito
        400:
            description: No se encontro el permiso en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        permiso = rbacWS.dbRepo.get_permiso(permiso_id)
        permiso = rbacWS.dbRepo.delete_permiso(permiso_id)
        return RbacWS.response_json(info="Eliminar permiso - Exito",data=permiso)
    except Exception as e:
        logger.error("{}".format(e))
        return RbacWS.response_json(info="Eliminar permiso - Error al eliminar permiso", code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/perfil/<perfil_id>/rol/<rol_id>',methods=['POST'])
def add_perfil_rol(perfil_id, rol_id):
    """
    Añadir un rol a un perfil
    ---
    tags:
        - rbac
        - perfil
    parameters:
        - in: path
          name: perfil_id
          type: string
          required: true
          description: uuid del perfil
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
    responses:
        200:
            description: Operacion exitosa al añadir rol al perfil
        400:
            description: No se encontro el perfil o el rol en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        perfil = rbacWS.dbRepo.get_perfil(perfil_id)
        if not perfil.buscar_rol(rol_id):
            rbacWS.dbRepo.add_perfil_rol(perfil_id, rol_id)
            return RbacWS.response_json(info="Registro rol del perfil - Exito")
        else:
            return RbacWS.response_json(info="Registro rol del perfil - Rol ya existente en el perfil", code=400)
    except Exception as e:
        logger.error("{}".format(e))
        return RbacWS.response_json(info="Registro rol - Error", code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/perfil/<perfil_id>/rol/<rol_id>',methods=['DELETE'])
def del_perfil_rol(perfil_id, rol_id):
    """
    Quitar un rol de un perfil
    ---
    tags:
        - rbac
        - perfil
    parameters:
        - in: path
          name: perfil_id
          type: string
          required: true
          description: uuid del perfil
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
    responses:
        200:
            description: Operacion exitosa al quitar rol al perfil
        400:
            description: No se encontro el perfil o el rol en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        perfil = rbacWS.dbRepo.get_perfil(perfil_id)
        if perfil.buscar_rol(rol_id):
            rbacWS.dbRepo.del_perfil_rol(perfil_id, rol_id)
            return RbacWS.response_json(info="Eliminar rol del perfil - Exito")
        else:
            return RbacWS.response_json(info="Eliminar rol del perfil - Rol no esta registrado en el perfil", code=400)
    except Exception as e:
        logger.error("{}".format(e))
        return RbacWS.response_json(info="Eliminar rol del perfil - Error", data=e,code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/perfil/<perfil_id>/rol',methods=['GET'])
def get_perfil_roles(perfil_id):
    """
    Obtener los roles asignados a un perfil
    ---
    tags:
        - rbac
        - perfil
    parameters:
        - in: path
          name: perfil_id
          type: string
          required: true
          description: uuid del perfil
    responses:
        200:
            description: Roles devueltos con exito
        400:
            description: No se encontro el perfil en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        perfil = rbacWS.dbRepo.get_perfil(perfil_id)
        roles = perfil.get_roles()
        roles_dict = []
        for rol in roles:
            roles_dict.append(rol.to_dict())
        return RbacWS.response_json(info="Obtener roles perfil - Exito", data= roles_dict)
    except:
        return RbacWS.response_json(info="Obtener roles perfil - Error", code=400)

@rbac_blueprint.route('/ws/sigaa_api/rbac/rol/<rol_id>/permiso/<permiso_id>',methods=['POST'])
def add_rol_permiso(rol_id, permiso_id):
    """
    Añadir un permiso a un rol
    ---
    tags:
        - rbac
        - rol
    parameters:
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
        - in: path
          name: permiso_id
          type: string
          required: true
          description: uuid del permiso
    responses:
        200:
            description: Permiso asignado al rol con exito
        400:
            description: No se encontro el rol o el perfil
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        rol = rbacWS.dbRepo.get_rol(rol_id)
        if not rol.buscar_permiso(permiso_id):
            rbacWS.dbRepo.add_rol_permiso(rol_id, permiso_id)
            return RbacWS.response_json(info="Registro permiso del rol - Exito")
        else:
            return RbacWS.response_json(info="Registro permiso del rol - permiso ya existente en el rol", code=400)
    except TypeError:
        return RbacWS.response_json(info="Registro permiso del rol - Error rol o permiso no existe", code=400)
    except Exception as e:
        logger.error("{}".format(e))
        return RbacWS.response_json(info="Registro permiso del rol - Error", code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/rol/<rol_id>/permiso/<permiso_id>',methods=['DELETE'])
def del_rol_permiso(rol_id, permiso_id):
    """
    Quitar un permiso a un rol
    ---
    tags:
        - rbac
        - rol
    parameters:
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
        - in: path
          name: permiso_id
          type: string
          required: true
          description: uuid del permiso
    responses:
        200:
            description: Permiso desasignado del rol con exito
        400:
            description: No se encontro el rol o el perfil
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        rol = rbacWS.dbRepo.get_rol(rol_id)
        if rol.buscar_permiso(permiso_id):
            rbacWS.dbRepo.del_rol_permiso(rol, permiso_id)
            return RbacWS.response_json(info="Quitar permiso de rol - Exito")
        else:
            return RbacWS.response_json(info="Quitar permiso de rol - Permiso no esta asignado en el rol", code=400)
    except TypeError:
        return RbacWS.response_json(info="Quitar permiso de rol - Error el rol no existe")
    except Exception as e:
        return RbacWS.response_json(info="Quitar permiso de rol - Error", code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/rol/<rol_id>/permiso',methods=['GET'])
def get_rol_permisos(rol_id):
    """
    Obtener los permisos de un rol
    ---
    tags:
        - rbac
        - rol
    parameters:
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
    responses:
        200:
            description: Permisos devueltos con exito
        400:
            description: No se encontro el rol.
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        rol = rbacWS.dbRepo.get_rol(rol_id)
        rol.get_permisos()
        return RbacWS.response_json(info="Obtener permiso del rol - Exito")
    except TypeError:
        return RbacWS.response_json(info="Obtener permiso del rol - Error Rol no existe", code=400)
    except Exception as e:
        return RbacWS.response_json(info="Quitar permiso de rol - Error", core=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/rol/<rol_id>/hijo/<hijo_id>',methods=['POST'])
def add_rol_hijo(rol_id, hijo_id):
    """
    Añadir un hijo a un rol
    ---
    tags:
        - rbac
        - rol
    parameters:
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
        - in: path
          name: hijo_id
          type: string
          required: true
          description: uuid del rol hijo
    responses:
        200:
            description: Rol hijo añadido con exito
        400:
            description: No se encontro el rol o el hijo.
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        rol = rbacWS.dbRepo.get_rol(rol_id)
        hijo = rbacWS.dbRepo.get_rol(hijo_id)
        rol.add_hijo(hijo)
        RbacPGSQL.update_rol(jsons.dumps(rol))
        return RbacWS.response_json(info="Añadir hijo a rol - Exito")
    except TypeError:
        return RbacWS.response_json(info="Añadir hijo a rol - rol o hijo no existe", code=400)
    except Exception as e:
        return RbacWS.response_json(info="Añadir hijo a rol - Error", code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/rol/<rol_id>/hijo/<hijo_id>',methods=['DELETE'])
def del_rol_hijo(rol_id, hijo_id):
    """
    desasignar un hijo a un rol
    ---
    tags:
        - rbac
        - rol
    parameters:
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
        - in: path
          name: hijo_id
          type: string
          required: true
          description: uuid del rol hijo
    responses:
        200:
            description: Rol hijo desasignado con exito
        400:
            description: No se encontro el rol o el hijo.
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        rol = rbacWS.dbRepo.get_rol(rol_id)
        hijo = rbacWS.dbRepo.get_rol(hijo_id)
        rol.remove_hijo_by_id(rol.get_id())
        RbacPGSQL.del_rol_hijo(rol_id, hijo_id)
        return RbacWS.response_json(info="Eliminar hijo de rol - Exito")
    except TypeError:
        return RbacWS.response_json(info="Eliminar hijo de rol - Rol o hijo no existe", code=400)
    except Exception as e:
        return RbacWS.response_json(info="Eliminar hijo de rol - Error", code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/permiso/<permiso_id>/hijo/<hijo_id>',methods=['POST'])
def add_permiso_hijo(permiso_id, hijo_id):
    """
    Añadir un hijo a un permiso
    ---
    tags:
        - rbac
        - permiso
    parameters:
        - in: path
          name: permiso_id
          type: string
          required: true
          description: uuid del permiso
        - in: path
          name: hijo_id
          type: string
          required: true
          description: uuid del permiso hijo
    responses:
        200:
            description: Permiso hijo añadido con exito
        400:
            description: No se encontro el permiso o el hijo.
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        permiso = rbacWS.dbRepo.get_rol(permiso_id)
        hijo = rbacWS.dbRepo.get_rol(hijo_id)
        permiso.add_hijo(hijo)
        RbacPGSQL.update_rol(jsons.dumps(permiso))
        return RbacWS.response_json(info="Añadir hijo a permiso - Exito")
    except TypeError:
        return RbacWS.response_json(info="Añadir hijo a permiso - permiso o hijo no existe", code=400)
    except Exception as e:
        return RbacWS.response_json(info="Añadir hijo a permiso - Error", code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/permiso/<permiso_id>/hijo/<hijo_id>',methods=['DELETE'])
def del_permiso_hijo(permiso_id, hijo_id):
    """
    desasignar un hijo a un permiso
    ---
    tags:
        - rbac
        - permiso
    parameters:
        - in: path
          name: permiso_id
          type: string
          required: true
          description: uuid del permiso
        - in: path
          name: hijo_id
          type: string
          required: true
          description: uuid del permiso hijo
    responses:
        200:
            description: Permiso hijo desasignado con exito
        400:
            description: No se encontro el permiso o el hijo.
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        permiso = rbacWS.dbRepo.get_permiso(permiso_id)
        hijo = rbacWS.dbRepo.get_permiso(hijo_id)
        permiso.remove_hijo_by_id(permiso.get_id())
        rbacWS.dbRepo.del_permiso_hijo(permiso_id, hijo_id)
        return RbacWS.response_json(info="Eliminar hijo de permiso - Exito")
    except TypeError:
        return RbacWS.response_json(info="Eliminar hijo de permiso - Permiso o hijo no existe", code=400)
    except Exception as e:
        return RbacWS.response_json(info="Eliminar hijo de permiso - Error", code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/contextos',methods=['GET'])
def get_contextos():
    """
    Obtener Contextos(Sistemas)
    ---
    tags:
        - rbac
        - contexto
    responses:
        200:
            description: Contextos recuperados con exito.
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    contextos = rbacWS.dbRepo.get_contextos()
    contextos_dict = []
    for contexto in contextos:
        contextos_dict.append(contexto.to_dict())
    res = RbacWS.response_json(data=contextos_dict)
    return res

@rbac_blueprint.route('/ws/sigaa_api/rbac/contexto/<contexto_id>',methods=['GET'])
def get_contexto(contexto_id):
    """
    Obtener un contexto por su uuid
    ---
    tags:
        - rbac
        - contexto
    parameters:
        - in: path
          name: contexto_id
          type: string
          required: true
          description: uuid del contexto
    responses:
        200:
            description: Contexto recuperado con exito
        400:
            description: No se encontro el contexto.
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        contexto = rbacWS.dbRepo.get_contexto(contexto_id)
        return RbacWS.response_json(info="Obtener contexto",data=contexto.to_dict())
    except TypeError:
        return RbacWS.response_json(info="Obtener contexto - no encontrado",data={}, code=400)
    except:
        return RbacWS.response_json(info="Obtener contexto - Error",data={}, code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/contexto/<contexto_id>/rol/<rol_id>',methods=['PUT'])
def set_contexto_rol(self,contexto_id, rol_id):
    """
    Asignar un rol de un contexto
    ---
    tags:
        - rbac
        - contexto
    parameters:
        - in: path
          name: contexto_id
          type: string
          required: true
          description: uuid del contexto
        - in: path
          name: rol_id
          type: string
          required: true
          description: uuid del rol
    responses:
        200:
            description: Rol asignado al contexto con exito
        400:
            description: No se encontro el contexto o el rol.
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        res = rbacWS.dbRepo.set_contexto_rol(contexto_id, rol_id)
        return rbacWS.response_json(info="Set contexto rol - Exito")
    except TypeError:
        return rbacWS.response_json(info="Set contexto rol - no encontrado",data={},code=400)
    except:
        return rbacWS.response_json(info="Set contexto rol - Error",data={}, code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/contexto/<contexto_id>/rol/<rol_id>',methods=['PUT'])
def set_contexto_permiso(self, contexto_id, permiso_id):
    """
    Asignar un permiso de un contexto
    ---
    tags:
        - rbac
        - contexto
    parameters:
        - in: path
          name: contexto_id
          type: string
          required: true
          description: uuid del contexto
        - in: path
          name: permiso_id
          type: string
          required: true
          description: uuid del permiso
    responses:
        200:
            description: Permiso asignado al contexto con exito
        400:
            description: No se encontro el contexto o el rol.
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    try:
        res = rbacWS.dbRepo.set_contexto_permiso(contexto_id, permiso_id)
        return rbacWS.response_json(info="Set contexto permiso - Exito")
    except TypeError:
        return rbacWS.response_json(info="Set contexto permiso - no encontrado",data={},code=400)
    except:
        return rbacWS.response_json(info="Set contexto permiso - Error",data={}, code=500)

@rbac_blueprint.route('/ws/sigaa_api/rbac/contexto',methods=['POST'])
def crear_contexto():
    """
    Crear contexto
    ---
    tags:
        - rbac
        - contexto
    consumes:
        - application/json
    parameters:
        - name: body
          in: body
          required: true
          schema:
            required:
                - nombre
            properties:
                nombre:
                    type: string
                    description: el nombre del contexto
                configuracion:
                    type: object
                    description: la configuracion del contexto
            example:
                nombre: sistema_1
                configuracion: {"esquemas_configuracion":{
                    "perfil":{"password":string},
                    "rol":{
                        "periodo":{"fecha_inicio":date,"fecha_fin":date},
                        "habilitado":bool
                    },
                    "permiso":{
                        "periodo":{"fecha_inicio":date,"fecha_fin":date},
                        "pago":bool
                        }
                    }
                }
    responses:
        200:
            description: Devuelve el perfil
        400:
            description: No se encontro el perfil en la base de datos
        500:
            description: Error en el motor de persistencia o el servidor
    """
    rbacWS = RbacWS()
    data = request.get_json()
    try:
        contexto = rbacWS.dbRepo.crear_contexto(data["nombre"])
        print(data)
        if data["configuracion"]:
            print(data["configuracion"])
            contexto.set_configuracion(data["configuracion"])
            rbacWS.dbRepo.update_contexto(contexto.to_json())
        return rbacWS.response_json(info="Crear contexto - Exito")
    except TypeError as e:
        return rbacWS.response_json(info="Crear contexto - datos no validos",data=e,code=400)
    except Exception as e:
        logger.error(data,e)
        return rbacWS.response_json(info="Crear contexto - Error",data=e, code=500)
    
