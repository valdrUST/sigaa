#!/usr/bin/env python
import json

class Contexto():
    """
    Clase Contexto
    """
    def __init__(self, id, nombre):
        self.id = id
        self.nombre = nombre
        self.configuracion = {}
        self.permisos = []
        self.roles = []
    
    def set_id(self, id):
        self.id = id
    
    def get_id(self):
        return self.id
    
    def get_nombre(self):
        return self.nombre
    
    def get_configuracion(self):
        return self.configuracion
    
    def set_configuracion(self, configuracion):
        self.configuracion = configuracion

    def get_permisos(self):
        return self.permisos
    
    def set_permiso(self, permiso):
        if permiso not in self.permisos:
            for p in self.permisos:
                if permiso.is_padre(p):
                    return False
            self.permisos.append(permiso)
            permiso.set_contexto(self)
            return permiso
        return False

    def remove_permiso(self, permiso):
        self.permisos.remove(permiso)
    
    def get_roles(self):
        return self.roles

    def set_rol(self, rol):
        if rol not in self.roles:
            for r in self.roles:
                if rol.is_padre(r):
                    return False
            self.roles.append(rol)
            rol.set_contexto(self)
            return rol
        return False


    def remove_rol(self, rol):
        self.roles.remove(rol)
    
    def to_dict(self):
        contexto_dict = {}
        roles_dict = []
        permisos_dict = []
        contexto_dict["id"] = self.get_id()
        contexto_dict["nombre"] = self.get_nombre()
        contexto_dict["configuracion"] = self.get_configuracion()
        for r in self.get_roles():
            roles_dict.append(r.to_dict())
        for p in self.get_permisos():
            permisos_dict.append(p.to_dict())
        contexto_dict["roles"] = roles_dict
        contexto_dict["permisos"] = permisos_dict
        return contexto_dict
    
    def to_json(self):
        return json.dumps(self.to_dict())

from sigaa_api.shared.dominio.Repository import Repository
class ContextoRepo(Repository):
    def __init__(self):
        Repository.__init__(self)
    
    def delete(self, id):
        old = self.search(id)
        if old:
            self.list.remove(old)
            return True
        return False
    
    def update(self, new):
        old = self.search(new.get_id())
        if old:
            self.list.remove(old)
            self.list.append(new)
            return True
        return False

    def search(self, id):
        for c in self.list:
            if c.get_id() == id:
                return c
        return False
