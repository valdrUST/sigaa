#!/usr/bin/env python
from flask import Flask, escape, request, Blueprint, jsonify, make_response
import jsons
from sigaa_api.rbac.aplicacion.RbacAPP import RbacAPP
from sigaa_api.rbac.infraestructura.RbacPGSQL import RbacPGSQL
from flask_jwt import JWT, jwt_required, current_identity
from werkzeug.security import safe_str_cmp
from sigaa_api.shared.infraestructura.Logger import logger

auth_blueprint = Blueprint('auth', __name__)

class AuthWS(object):
    """
    Clase AuthWS, sirve para crear el blueprint para flask
    """
    def __init__(self, key="secret", algorithm='HS256'):
        self.key = key
        self.algorithm = algorithm
        self.dbRepo = RbacPGSQL()
    
    def authenticate(self, perfil_id, password):
        user = self.dbRepo.get_perfil(perfil_id)
        if user:
            return user
    
    def identity(self,payload):
        perfil_id = payload['id']
        user = self.dbRepo.get_perfil(perfil_id)
        payload['rol'] = user.get_roles()
        return user.get_id()

    @staticmethod
    def response_json(info="AuthWS",data={},code=200):
        """
        Plantilla para las respuestas RbacWS
        """
        response = make_response(jsons.dumps({
            "info":info,
            "code":code,
            "data":data
        }), code)
        response.headers["Content-Type"] = "application/json"
        return response
    
    

