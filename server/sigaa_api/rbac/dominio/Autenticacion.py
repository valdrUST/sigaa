#!/usr/bin/env python
import json
from sigaa_api.rbac.dominio.Rol import Rol

class Autenticacion(object):
    """
    Clase Autenticacion
    """
    def __init__(self, id):
        self.id = id
        self.info = {}
        self.roles = []
    
    def set_id(self, id):
        self.id = id
    
    def get_id(self):
        return self.id
    
    def set_info(self,info:dict):
        self.info = info
    
    def add_info(self,key:str,valor):
        self.info[key] = valor
    
    def remove_info(self,key):
        del self.info[key]
    
    def get_info(self):
        return self.info
    
    def buscar_rol(self, rol_id):
        roles = self.get_roles()
        for rol in roles:
            if rol.get_id() == rol_id:
                return rol
        return False
    
    def add_rol(self, rol:Rol):
        if rol not in self.roles:
            for r in self.roles:
                if rol.is_padre(r):
                    return False
            self.roles.append(rol)

    def remove_rol(self, rol):
        self.roles.remove(rol)
    
    def get_roles(self):
        return self.roles

    def to_dict(self):
        autenticacion_dict = {}
        roles_dict = []
        autenticacion_dict["id"] = self.get_id()
        autenticacion_dict["info"] = self.get_info()
        for r in self.get_roles():
            roles_dict.append(r.to_dict())
        autenticacion_dict["roles"] = roles_dict
        return autenticacion_dict
    
    def to_json(self):
        return json.dumps(self.to_dict())

from sigaa_api.shared.dominio.Repository import Repository
class AutenticacionRepo(Repository):
    """
    Repositorio de permisos
    """
    def __init__(self):
        Repository.__init__(self)
    
    def delete(self, id):
        old = self.search(id)
        if old:
            self.list.remove(old)
            return True
        return False
    
    def update(self, new):
        old = self.search(new.get_id())
        if old:
            self.list.remove(old)
            self.list.append(new)
            return True
        return False

    def search(self, id):
        for a in self.list:
            if a.get_id() == id:
                return a
        return False
