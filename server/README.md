# sigaa_api
## Instrucciones de uso
* para correr el servidor usando el WSGI
```bash
$ sigaa_apiWSGI
```
* para correr en modo consola para desarrollo y debug
```bash
$ sigaa_api --ws --port 8001 --host 0.0.0.0
```
