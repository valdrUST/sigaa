#!/usr/bin/env python
from configparser import ConfigParser
from sigaa_api.shared.infraestructura.Logger import logger

class Config(object):
    """
    Clase para las configuraciones
    """
    @staticmethod
    def config_ini(iniFile, section):
        """
        iniFile: archivo de configuracion tipo ini
        section: Seccion del archivo de configuracion a procesar
        """
        configData = {}
        parser = ConfigParser()
        parser.read(iniFile)
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                configData[param[0]] = param[1]
        else:
            logger.warning("Error en el archivo de configuracion {}".format(iniFile))
            raise Exception("Error en el archivo de configuracion {}".format(iniFile))
        return configData
            

