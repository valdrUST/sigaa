#!/usr/bin/env python
import json
import uuid

from sigaa_api.rbac.dominio.Autenticacion import Autenticacion, AutenticacionRepo
from sigaa_api.rbac.dominio.Rol import Rol, RolRepo
from sigaa_api.rbac.dominio.Permiso import Permiso, PermisoRepo
from sigaa_api.rbac.dominio.Contexto import Contexto, ContextoRepo
from sigaa_api.shared.infraestructura.Logger import logger

class RbacAPP(object):
    """
    Aplicacion para los casos de uso para la RBAC
    """
    def __init__(self):
        self.autenticacion_repo = AutenticacionRepo()
        self.rol_repo = RolRepo()
        self.permiso_repo = PermisoRepo()
        self.contexto_repo = ContextoRepo()
    
    def gen_id(self):
        return str(uuid.uuid1())

    def crear_perfil(self):
        """
        Crear el perfil de autenticacion
        """
        perfil = Autenticacion(self.gen_id())
        self.autenticacion_repo.insert(perfil)
        try:
            self.crear_perfil_persist(perfil)
        except:
            return perfil
    
    def crear_rol(self, nombre):
        """
        Crear nuevo rol
        """
        rol = Rol(self.gen_id(),nombre=nombre)
        self.rol_repo.insert(rol)
        return rol
    
    def crear_permiso(self, nombre):
        """
        Crear permiso
        """
        permiso = Permiso(self.gen_id(), nombre=nombre)
        self.permiso_repo.insert(permiso)
        return permiso
    
    def add_perfil(self, perfil: Autenticacion):
        """
        Añadir un perfil al repo de la aplicacion
        """
        return self.autenticacion_repo.insert(perfil)
    
    def add_rol(self, rol: Rol):
        """
        Añadir un rol al repo de la aplicacion
        """
        return self.rol_repo.insert(rol)
    
    def add_permiso(self, permiso:Permiso):
        """
        Añadir un permiso al repo de la aplicacion
        """
        return self.permiso_repo.insert(permiso)
    
    def update_perfil(self, perfil:Autenticacion):
        """
        Hacer update a elemento del perfil
        """
        return self.autenticacion_repo.update(perfil)
    
    def update_rol(self, rol:Rol):
        """
        Hacer update a elemento del rol
        """
        return self.rol_repo.update(rol)
    
    def update_permiso(self, permiso:Permiso):
        """
        Hacer update a elemento del permiso
        """
        return self.permiso_repo.update(permiso)

    def delete_perfil(self, perfil_id:str):
        """
        Eliminar un perfil del repo de la aplicacion
        """
        return self.autenticacion_repo.delete(perfil_id)
    
    def delete_rol(self, rol_id:str):
        """
        Eliminar rol y todos los hijos del repo de la aplicacion
        """
        rol = self.rol_repo.search(rol_id)
        huerfanos = rol.remove_hijos()
        self.rol_repo.delete(rol_id)
        for huerfano in huerfanos:
            if not self.rol_repo.update(huerfano):
                return False
        return True
    
    def delete_permiso(self, permiso_id:str):
        """
        Eliminar permiso y todos los hijos del repo de la aplicacion
        """
        permiso = self.permiso_repo.search(permiso_id)
        huerfanos = permiso.remove_hijos()
        self.permiso_repo.delete(permiso_id)
        for huerfano in huerfanos:
            if not self.permiso_repo.update(huerfano):
                return False
        return True
    
    def get_perfil(self, perfil_id:str):
        """
        Obtener perfil por id
        """
        perfil = self.autenticacion_repo.search(perfil_id)
        return perfil
    
    def get_rol(self, rol_id:str):
        """
        Obtener rol por id
        """
        rol = self.rol_repo.search(rol_id)
        return rol
    
    def get_permiso(self, permiso_id:str):
        """
        Obtener pemiso por id
        """
        permiso = self.permiso_repo.search(permiso_id)
        return permiso
        
    def add_perfil_rol(self, perfil_id:str, rol_id:str):
        """
        Añadir un rol nuevo a un perfil
        """
        perfil = self.autenticacion_repo.search(perfil_id)
        rol = self.rol_repo.search(rol_id)
        perfil.add_rol(rol)
        return self.autenticacion_repo.update(perfil)
    
    def del_perfil_rol(self, perfil_id: str, rol_id :str):
        """
        Añadir un rol nuevo a un perfil
        """
        perfil = self.autenticacion_repo.search(perfil_id)
        rol = self.rol_repo.search(rol_id)
        perfil.remove_rol(rol)
        self.autenticacion_repo.update(perfil)
    
    def add_rol_hijo(self, rol_id :str, hijo_id:str):
        """
        Añade un hijo a un rol
        """
        rol = self.rol_repo.search(rol_id)
        hijo = self.rol_repo.search(hijo_id)
        rol.add_hijo(hijo)
        self.rol_repo.update(rol)
        self.rol_repo.update(hijo)
    
    def del_rol_hijo(self, rol_id: str, hijo_id: str):
        """
        Elimina un hijo de un rol
        """
        rol = self.rol_repo.search(rol_id)
        hijo = self.rol_repo.search(hijo_id)
        rol.remove_hijo_by_id(hijo.get_id())
        self.rol_repo.update(rol)
        self.rol_repo.update(hijo)
    
    def add_permiso_hijo(self, permiso_id:str, hijo_id:str):
        """
        Añade un hijo de un permiso
        """
        permiso = self.permiso_repo.search(permiso_id)
        hijo = self.permiso_repo.search(hijo_id)
        permiso.add_hijo(hijo)
        self.permiso_repo.update(permiso)
        self.permiso_repo.update(hijo)
    
    def del_permiso_hijo(self, permiso_id:str, hijo_id:str):
        """
        Elimina un hijo de un permiso
        """
        permiso = self.permiso_repo(permiso_id)
        hijo = self.permiso_repo(hijo_id)
        permiso.remove_hijo_by_id(hijo.get_id())
        self.permiso_repo.update(permiso)
        self.permiso_repo.update(hijo)

    def add_rol_permiso(self, rol_id:str, permiso_id:str):
        """
        Añade un permiso a un rol
        """
        rol = self.rol_repo.search(rol_id)
        permiso = self.permiso_repo.search(permiso_id)
        rol.add_permiso(permiso)
        self.rol_repo.update(rol)
        return rol
    
    def del_rol_permiso(self, rol_id:str, permiso_id:str):
        """
        Elimina un permiso de un rol
        """
        rol = self.rol_repo.search(rol_id)
        permiso = self.permiso_repo.search(permiso_id)
        rol.remove_permiso(permiso)
        self.rol_repo.update(rol)
        return rol
    
    def get_contexto(self, contexto_id):
        """
        Obtiene un contexto del repo
        """
        contexto = self.contexto_repo.search(contexto_id)
        return contexto
    
    def set_contexto_rol(self,contexto, rol):
        if contexto.set_rol(rol):
            return contexto
        else:
            return False
    
    def set_contexto_permiso(self, contexto, permiso):
        if contexto.set_permiso(permiso):
            return contexto
        else:
            return False
    
    def crear_contexto(self, nombre):
        contexto = Contexto(self.gen_id(),nombre)
        if self.contexto_repo.insert(contexto):
            return contexto
        return False

    def poblar_rol_repo(self, data, tipo="json"):
        self.rol_repo = RolRepo()
        if tipo == "json":
            rol_array = json.loads(data)
        else:
            raise Exception("Tipo de dato no es valido")
        for r in rol_array:
            self.rol_repo.insert(self.deserialize_json_rol(json.dumps(r)))
    
    def poblar_permiso_repo(self, data, tipo="json"):
        self.permiso_repo = PermisoRepo()
        if tipo == "json":
            permiso_array = json.loads(data)
        else:
            raise Exception("Tipo de dato no es valido")
        for p in permiso_array:
            self.permiso_repo.insert(self.deserialize_json_permiso(json.dumps(p)))
    
    def poblar_autenticacion_repo(self, data, tipo="json"):
        self.autenticacion_repo = AutenticacionRepo()
        if tipo == "json":
            autenticacion_array = json.loads(data)
        else:
            raise Exception("Tipo de dato no es valido")
        for a in autenticacion_array:
            self.autenticacion_repo.insert(self.deserialize_json_autenticacion(json.dumps(a)))

    def poblar_contexto_repo(self, data, tipo="json"):
        self.contexto_repo = ContextoRepo()
        if tipo == "json":
            contexto_array = json.loads(data)
        else:
            raise Exception("Tipo de dato no es valido")
        for c in contexto_array:
            self.contexto_repo.insert(self.deserialize_json_contexto(json.dumps(c)))
            
    def serialize_rol_json(self, rol : Rol):
        """
        Pasa del objeto Rol a JSON
        """
        return json.dumps(rol.to_dict())

    def serialize_permiso_json(self, permiso : Permiso):
        """
        Pasa del objeto Permiso a JSON
        """
        return json.dumps(permiso.to_dict())
    
    def serialize_autenticacion_json(self, autenticacion : Autenticacion):
        """
        Pasa del objeto Autenticacion a JSON
        """
        return json.dumps(autenticacion.to_dict())

    def deserialize_json_rol(self, json_data):
        """
        Pasa de JSON a objeto Rol
        """
        if type(json_data) == str:
            data = json.loads(json_data)
        else:
            data = json_data
        r = Rol(data["id"], data["nombre"])
        r.set_configuracion(data["configuracion"])
        if data["permisos"] != None:
            for permiso in data["permisos"]:
                p = self.deserialize_json_permiso(permiso)
                r.add_permiso(p)
        if data["hijos"] != None:
            for hijo in data["hijos"]:
                hijo = self.deserialize_json_rol(json.dumps(hijo))
                r.add_hijo(hijo)
        return r
    
    def deserialize_json_permiso(self, json_data):
        """
        pasa de JSON a objeto Permiso
        """
        if type(json_data) == str:
            data = json.loads(json_data)
        else:
            data = json_data
        p = Permiso(data["id"], data["nombre"])
        p.set_configuracion(data["configuracion"])
        if data["hijos"] != None:
            for hijo in data["hijos"]:
                hijo = self.deserialize_json_permiso(json.dumps(hijo))
                p.add_hijo(hijo)
        return p

    def deserialize_json_autenticacion(self, json_data):
        """
        Pasa de objeto JSON a Autenticacion
        """
        if type(json_data) == str:
            data = json.loads(json_data)
        else:
            data = json_data
        a = Autenticacion(data["id"])
        a.set_info(data["info"])
        if None not in data["roles"]:
            for rol in data["roles"]:
                a.add_rol(self.deserialize_json_rol(rol))
        return a
    
    def deserialize_json_contexto(self, json_data):
        """
        Pasa de objeto JSON a Contexto
        """
        if type(json_data) == str:
            data = json.loads(json_data)
        else:
            data = json_data
        c = Contexto(data["id"], data["nombre"])
        c.set_configuracion(data["configuracion"])
        if None not in data["roles"]:
            for rol in data["roles"]:
                c.add_rol(self.deserialize_json_rol(rol))
        if None not in data["permisos"]:
            for permiso in data["permisos"]:
                c.add_rol(self.deserialize_json_permiso(permiso))
        return c

"""
Este __main__ solo son pruebas
"""
if __name__ == "__main__":
    app = RbacAPP()
    a1 = app.crear_perfil()
    a2 = app.crear_perfil()
    a1.set_info({
        "usuario":"kekito",
        "email":"kekito@kekito.com"
    })
    a2.add_info("usuario","root")
    app.update_perfil(a1)
    app.update_perfil(a2)

    p1 = app.crear_permiso("permiso 1.0")
    p2 = app.crear_permiso("permiso 2.0")
    p2_1 = app.crear_permiso("permiso 2.1")
    p3 = app.crear_permiso("permiso 3.0")
    configuracionP3 = {
        "periodo_inicial": "2020-12-10 09:00:00",
        "periodo_final" : "2020-12-10 21:00:00"
    }
    p2 = p1.add_hijo(p2)
    p2_1 = p1.add_hijo(p2_1)
    p3 = p2.add_hijo(p3)
    p3.set_configuracion(configuracionP3)

    app.update_permiso(p1)
    app.update_permiso(p2)
    app.update_permiso(p2_1)
    app.update_permiso(p3)
    #print(json.dumps(p1.to_dict()))
    r1 = app.crear_rol("rol 1.0")
    r2 = app.crear_rol("rol 2.0")
    r3 = app.crear_rol("rol 3.0")
    r3_1 = app.crear_rol("rol 3.1")
    configuracionR3 = {
        "id_pago": "pagos_curso_diplo"
    }
    r3_1.set_configuracion(configuracionR3)
    r1.add_permiso(p1)
    r2 = r1.add_hijo(r2)
    r3_1 = r2.add_hijo(r3_1)
    r2.add_permiso(p2)
    r3 = r2.add_hijo(r3)
    #print(json.dumps(r1.to_dict()))
    a1_new = app.get_perfil(a1.get_id())
    a2_new = app.get_perfil(a2.get_id())
    a1_new.add_rol(r1)
    a2_new.add_rol(r2)
    app.update_perfil(a1)
    app.update_perfil(a2)
    #print(json.dumps(a1.to_dict()))
    #print(json.dumps(a2.to_dict()))
    p1 = app.get_permiso(p1.get_id())
    Serialize = app.serialize_permiso_json(p1)
    Deserialize = app.deserialize_json_permiso(Serialize)
    Serialize2 = app.serialize_permiso_json(Deserialize)
    print(Serialize == Serialize2)
    Serialize = app.serialize_rol_json(r1)
    Deserialize = app.deserialize_json_rol(Serialize)
    Serialize2 = app.serialize_rol_json(Deserialize)
    print(Serialize == Serialize2)
    Serialize = app.serialize_autenticacion_json(a1)
    Deserialize = app.deserialize_json_autenticacion(Serialize)
    Serialize2 = app.serialize_autenticacion_json(Deserialize)
    print(Serialize == Serialize2)
    json_repo = app.serialize_autenticacion_repo_json()
    app.deserialize_json_autenticacion_repo(json_repo)
