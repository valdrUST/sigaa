#!/usr/bin/env python
import json
from sigaa_api.rbac.dominio.Arbol import Arbol
from sigaa_api.rbac.dominio.Permiso import Permiso
from sigaa_api.rbac.dominio.Contexto import Contexto
from sigaa_api.shared.dominio.Repository import Repository
class Rol(Arbol):
    """
    Clase Rol
    """
    def __init__(self, id, nombre):
        Arbol.__init__(self,id)
        self.nombre = nombre
        self.contexto = None
        self.configuracion = {}
        self.permisos = []
        self.hijos = []
        self.padre = None
    
    def get_id(self):
        """
        getter id
        """
        return self.id
    
    def set_id(self, id):
        self.id = id
    
    def get_nombre(self):
        return self.nombre
    
    def set_nombre(self,nombre):
        self.nombre = nombre

    def set_configuracion(self,configuracion):
        self.configuracion = configuracion

    def get_configuracion(self):
        return self.configuracion

    def add_configuracion(self,key,valor):
        self.configuracion[key] = valor
    
    def remove_configuracion(self,key):
        del self.configuracion[key]
    
    def add_permiso(self, permiso):
        if permiso not in self.permisos:
            for p in self.permisos:
                if permiso.is_padre(p):
                    return False
            self.permisos.append(permiso)

    def remove_permiso(self, permiso):
        self.permisos.remove(permiso)
    
    def get_permisos(self):
        return self.permisos
    
    def buscar_permiso(self, permiso_id):
        for permiso in self.get_permisos():
            if permiso.get_id() == permiso_id:
                return permiso
        return False

    def set_contexto(self, contexto:Contexto):
        self.contexto = contexto
        for hijo in self.hijos:
            hijo.set_contexto(contexto)
        for permiso in self.permisos:
            permiso.set_contexto(contexto)
        
    def get_contexto(self):
        return self.contexto    

    def to_dict(self):
        rol_dict = {}
        hijos_dict = []
        permisos_dict = []
        rol_dict["id"] = self.get_id()
        rol_dict["nombre"] = self.get_nombre()
        rol_dict["configuracion"] = self.get_configuracion()
        for r in self.get_hijos():
            hijos_dict.append(r.to_dict())
        if len(hijos_dict) > 0:
            rol_dict["hijos"] = hijos_dict
        else:
            rol_dict["hijos"] = None
        for p in self.get_permisos():
            permisos_dict.append(p.to_dict())
        if len(permisos_dict) > 0:
            rol_dict["permisos"] = permisos_dict
        else:
            rol_dict["permisos"] = None
        return rol_dict
    
    def to_json(self):
        return json.dumps(self.to_dict())

class RolRepo(Repository):
    """
    Repositorio de permisos
    """
    def __init__(self):
        Repository.__init__(self)
    
    def delete(self, id):
        old = self.search(id)
        if old:
            self.list.remove(old)
            return True
        return False
    
    def update(self, new):
        old = self.search(new.get_id())
        if old:
            self.list.remove(old)
            self.list.append(new)
            return True
        return False

    def search(self, id) -> Rol:
        for r in self.list:
            if r.get_id() == id:
                return r
        return False
