#!/usr/bin/env python
from flask import Flask, escape, request, Blueprint, jsonify, make_response, redirect
from os import mkdir, path
import base64
import json
import tempfile
import logging
from flask_socketio import SocketIO, send, emit
from flask_cors import CORS
from flask_jwt import JWT, JWTError
from datetime import datetime, timedelta
from flasgger import Swagger
from threading import Lock
from sigaa_api.rbac.infraestructura.RbacWS import rbac_blueprint
from sigaa_api.rbac.infraestructura.AuthWS import auth_blueprint, AuthWS
from .Controller import Controller
from sigaa_api.shared.infraestructura.Logger import logger

index_blueprint = Blueprint('index', __name__)
control = Controller()
io = SocketIO()
jwt = None
swagger = None
class Server():
    def __init__(self, name, *args, **kwargs):
        global jwt
        global io
        global swagger
        authWS = AuthWS()
        self.app = Flask(name)
        self.app.register_blueprint(index_blueprint)
        self.app.register_blueprint(rbac_blueprint)
        self.app.register_blueprint(auth_blueprint)
        self.app.config["SECRET_KEY"] = "super-secret"
        self.app.config['JWT_AUTH_HEADER_NAME'] = 'JWTAuthorization'
        self.app.config['JWT_AUTH_HEADER_PREFIX'] = 'Bearer'
        self.app.config["JWT_AUTH_URL_RULE"] = "/ws/sigaa_api/auth"
        self.cors = CORS(self.app,resources={r"/*":{"origins":"*"}})
        swagger_config = {
            "info": {
                "title" : "Leviatan API",
                "version" : "0.1.0"
            },
            "headers": [
            ],
            "specs": [
            {
                "endpoint": 'leviatan_spec',
                "route": '/ws/sigaa_api/leviatan_spec.json',
                "rule_filter": lambda rule: True,  # all in
                "model_filter": lambda tag: True,  # all in
            }
            ],
            "static_url_path": "/flasgger_static",
            # "static_folder": "static",  # must be set by user
            "swagger_ui": True,
            "specs_route": "/ws/sigaa_api/docs"
        }
        self.swagger = Swagger(self.app,config=swagger_config)
        self.io = SocketIO(self.app)
        self.io.init_app(self.app, cors_allowed_origins="*")
        swagger = self.swagger 
        io = self.io
        jwt = JWT(self.app, authWS.authenticate, authWS.identity)
        
        @jwt.jwt_payload_handler
        def make_payload(identity):
            rol_json = []
            roles = identity.get_roles()
            for rol in roles:
                rol_json.append(rol.to_dict())
            return {'iat':datetime.utcnow(),
                    'nbf':datetime.utcnow(),
                    'exp':datetime.utcnow() + timedelta(minutes=15),
                    'id': identity.id,
                    'rbac': rol_json
                    }

        def jwt_request_handler():
            auth_header_name = self.app.config['JWT_AUTH_HEADER_NAME']
            auth_header_value = request.headers.get(auth_header_name, None)
            auth_header_prefix = self.app.config['JWT_AUTH_HEADER_PREFIX']
            if not auth_header_value:
                return

            parts = auth_header_value.split()

            if parts[0].lower() != auth_header_prefix.lower():
                raise JWTError('Invalid JWT header', 'Unsupported authorization type')
            elif len(parts) == 1:
                raise JWTError('Invalid JWT header', 'Token missing')
            elif len(parts) > 2:
                raise JWTError('Invalid JWT header', 'Token contains spaces')
            return parts[1]
        
        jwt.request_handler(jwt_request_handler)

        @self.app.errorhandler(400)
        def handle_400(e):
            print(e)
            return jsonify({"info":"{}".format(e),
                            "code":400}), 400
        
        @self.app.errorhandler(404)
        def handle_404(e):
            print(e)
            return jsonify({"info":"Considere el root de la api /ws/sigaa_api\n{}".format(e),
                            "code":404}), 404
        @self.app.errorhandler(405)
        def handle_405(e):
            return jsonify({"info":"{}".format(e),
                            "code":405}), 405
        
        @self.app.errorhandler(500)
        def handle_500(e):
            return jsonify({"info":"{}".format(e),
                            "code":500}), 500
    
    @staticmethod
    def response_json(info="sigaa_apiWS",data={},code=200, headers=None):
        """
        Plantilla para las respuestas RbacWS
        """
        response = make_response(jsonify({
            "info":info,
            "code":code,
            "data":data
        }), code)
        response.headers["Content-Type"] = "application/json"
        if headers:
            print(headers)
            response.headers.extend(headers)
        return response
    
    @staticmethod
    @auth_blueprint.route("/ws/sigaa_api/login", methods=["POST"])
    def login():
        """
        Autenticacion para generar JWS
        ---
        tags:
            - JWT
        consumes:
            - application/json
        parameters:
            - in: body
              name: body
              required: true
              schema:
                required:
                    - id
                    - password
                properties:
                    autenticacion:
                        type: object
                        description: json de la autenticacion
                        required: true
                example:
                    autenticacion: {"id":"c346c2b8-0b0f-11eb-9218-0242ac130002",
                        "password":"contraseña123"}
                    
        responses:
            200:
                description: Autenticacion creada con exito
            400:
                description: Error
            500:
                description: Error en el motor de persistencia o el servidor
        """
        data = request.get_json()
        auth = AuthWS()
        perfil = auth.authenticate(data["autenticacion"]["id"],data["autenticacion"]["password"])
        if not perfil:
            return Server.response_json(info="login - Usuario o contraseña no validos",code=401)
        access_token = jwt.jwt_encode_callback(perfil)
        return Server.response_json(info="Exito", data=data, headers={'jwt-token': access_token})

@index_blueprint.route('/ws/sigaa_api',methods=['GET'])
def index():
    """
    Index endpoint, te redirigira a /ws/sigaa_api/docs
    ---
    tags:
        - index
    responses:
        200:
            description: OK
    """
    logging.info("index")
    io.emit("index",{"data":"sigaa_api socket.ip"},namespace='/sigaa_api')

    return redirect("/ws/sigaa_api/docs")
