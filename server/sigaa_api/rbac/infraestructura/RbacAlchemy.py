from sigaa_api.shared.infraestructura.DatabaseAlchemy import Base, engine, session
from sigaa_api.rbac.dominio.Permiso import Permiso
from sqlalchemy import Column, Integer, String, Float, JSON, ForeignKey, Table
from sqlalchemy.orm import relationship
from sigaa_api.shared.infraestructura.Logger import logger
Base.metadata.clear()
class UsuarioAlchemy(Base):
    __tablename__ = "usuario"
    id = Column(String, primary_key=True)
    nombre = Column(String, nullable=False)
    info = Column(JSON)
    def __init__(self):
        pass

permiso_hijo_tabla = Table("permiso_hijo", Base.metadata,
    Column("padre_id", String, ForeignKey("permiso.id")),
    Column("hijo_id", String, ForeignKey("permiso.id"), unique=True)
    )

class PermisoAlchemy(Base):
    __tablename__ = "permiso"
    id = Column(String, primary_key=True)
    nombre = Column(String, nullable=False)
    configuracion = Column(JSON)
    padre_id = Column(String, ForeignKey('permiso.id'))
    padre = relationship("permiso", back_populates="permiso")
    hijo = relationship("permiso",
        secondary=permiso_hijo_tabla)

    def __init__(self, id, nombre, configuracion, padre_id):
        self.id = id
        self.nombre = nombre
        self.configuracion = configuracion
        self.padre_id = padre_id

rol_hijo_tabla = Table("rol_hijo", Base.metadata,
    Column("padre_id", String, ForeignKey("rol.id")),
    Column("hijo_id", String, ForeignKey("rol.id"), unique=True)
    )

rol_permiso_tabla = Table("rol_permiso", Base.metadata,
    Column("rol_id", String, ForeignKey("rol.id"), primary_key=True),
    Column("permiso_id", String, ForeignKey("permiso.id"), primary_key=True)
    )

class RolAlchemy(Base):
    __tablename__ = "rol"
    id = Column(String, primary_key=True)
    nombre = Column(String, nullable=False)
    configuracion = Column(JSON)
    padre_id = Column(String, ForeignKey('rol.id'))
    padre = relationship("rol", back_populates="rol")
    hijo = relationship("rol",
        secondary=rol_hijo_tabla)
    permiso = relationship("permiso", back_populates="permiso",
        secondary=rol_permiso_tabla)

    def __init__(self):
        pass

if __name__ == "__main__":
    Base.metadata.create_all(engine)
    pa = PermisoAlchemy(id="1", nombre="p1", configuracion="{}", padre_id=None)
    print(pa)