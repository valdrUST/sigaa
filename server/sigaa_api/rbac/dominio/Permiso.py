#!/usr/bin/env python
import json
from sigaa_api.rbac.dominio.Arbol import Arbol
from sigaa_api.rbac.dominio.Contexto import Contexto
from sigaa_api.shared.dominio.Repository import Repository
class Permiso(Arbol):
    """
    Clase permiso
    """
    def __init__(self, id, nombre):
        Arbol.__init__(self,id)
        self.nombre = nombre
        self.contexto = None
        self.padre = None
        self.configuracion = {}
        self.hijos = []
    
    def get_nombre(self):
        return self.nombre
    
    def set_nombre(self,nombre):
        self.nombre = nombre

    def set_configuracion(self,configuracion):
        self.configuracion = configuracion

    def get_configuracion(self):
        return self.configuracion
    
    def add_configuracion(self,key,valor):
        self.configuracion[key] = valor
    
    def remove_configuracion(self,key):
        del self.configuracion[key]

    def set_contexto(self, contexto:Contexto):
        self.contexto = contexto
        for hijo in self.hijos:
            hijo.set_contexto(contexto)

    def get_contexto(self):
        return self.contexto

    def to_dict(self):
        permiso_dict = {}
        hijos_dict = []
        permiso_dict["id"] = self.get_id()
        permiso_dict["nombre"] = self.get_nombre()
        permiso_dict["configuracion"] = self.get_configuracion()
        for p in self.get_hijos():
            hijos_dict.append(p.to_dict())
        if len(hijos_dict) > 0:
            permiso_dict["hijos"] = hijos_dict
        else:
            permiso_dict["hijos"] = None
        return permiso_dict
    
    def to_json(self):
        return json.dumps(self.to_dict())

class PermisoRepo(Repository):
    """
    Repositorio de permisos
    """
    def __init__(self):
        Repository.__init__(self)
    
    def delete(self, id):
        old = self.search(id)
        if old:
            self.list.remove(old)
            return True
        return False
    
    def update(self, new):
        old = self.search(new.get_id())
        if old:
            self.list.remove(old)
            self.list.append(new)
            return True
        return False

    def search(self, id):
        for p in self.list:
            if p.get_id() == id:
                return p
        return False
