--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0 (Debian 13.0-1.pgdg100+1)
-- Dumped by pg_dump version 13.0 (Debian 13.0-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- Database "leviatan" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0 (Debian 13.0-1.pgdg100+1)
-- Dumped by pg_dump version 13.0 (Debian 13.0-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: leviatan; Type: DATABASE; Schema: -;
--
--CREATE DATABASE leviatan WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';



\connect leviatan
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
--
-- Name: contexto; Type: TABLE; Schema: public; Owner: leviatan_dba
--

CREATE TABLE public.contexto (
    contexto_id character varying NOT NULL,
    nombre character varying NOT NULL,
    configuracion json
);


--
-- Name: permiso; Type: TABLE; Schema: public; Owner: leviatan_dba
--

CREATE TABLE public.permiso (
    permiso_id character varying NOT NULL,
    nombre character varying NOT NULL,
    configuracion json,
    padre_id character varying,
    contexto_id character varying
);


--
-- Name: permiso_hijo; Type: TABLE; Schema: public; Owner: leviatan_dba
--

CREATE TABLE public.permiso_hijo (
    padre_id character varying,
    hijo_id character varying,
    permiso_hijo_id character varying NOT NULL
);

--
-- Name: rol; Type: TABLE; Schema: public; Owner: leviatan_dba
--

CREATE TABLE public.rol (
    rol_id character varying NOT NULL,
    nombre character varying NOT NULL,
    configuracion json,
    padre_id character varying,
    contexto_id character varying
);

--
-- Name: rol_hijo; Type: TABLE; Schema: public; Owner: leviatan_dba
--

CREATE TABLE public.rol_hijo (
    padre_id character varying,
    hijo_id character varying,
    rol_hijo_id character varying NOT NULL
);



--
-- Name: rol_permiso; Type: TABLE; Schema: public; Owner: leviatan_dba
--

CREATE TABLE public.rol_permiso (
    rol_id character varying NOT NULL,
    permiso_id character varying NOT NULL,
    rol_permiso_id character varying NOT NULL
);


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: leviatan_dba
--

CREATE TABLE public.usuario (
    usuario_id character varying NOT NULL,
    info json
);



--
-- Name: usuario_rol; Type: TABLE; Schema: public; Owner: leviatan_dba
--

CREATE TABLE public.usuario_rol (
    usuario_rol_id character varying NOT NULL,
    usuario_id character varying NOT NULL,
    rol_id character varying NOT NULL
);


--
-- Name: contexto contexto_pk; Type: CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.contexto
    ADD CONSTRAINT contexto_pk PRIMARY KEY (contexto_id);


--
-- Name: permiso_hijo permiso_hijo_hijo_id_key; Type: CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.permiso_hijo
    ADD CONSTRAINT permiso_hijo_hijo_id_key UNIQUE (hijo_id);


--
-- Name: permiso_hijo permiso_hijo_pk; Type: CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.permiso_hijo
    ADD CONSTRAINT permiso_hijo_pk PRIMARY KEY (permiso_hijo_id);


--
-- Name: permiso permiso_pkey; Type: CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.permiso
    ADD CONSTRAINT permiso_pkey PRIMARY KEY (permiso_id);


--
-- Name: rol_hijo rol_hijo_pk; Type: CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.rol_hijo
    ADD CONSTRAINT rol_hijo_pk PRIMARY KEY (rol_hijo_id);


--
-- Name: rol_permiso rol_permiso_pk; Type: CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.rol_permiso
    ADD CONSTRAINT rol_permiso_pk PRIMARY KEY (rol_permiso_id);


--
-- Name: rol rol_pkey; Type: CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (rol_id);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (usuario_id);


--
-- Name: usuario_rol usuario_rol_pk; Type: CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT usuario_rol_pk PRIMARY KEY (usuario_rol_id);


--
-- Name: contexto_nombre_uindex; Type: INDEX; Schema: public; Owner: leviatan_dba
--

CREATE UNIQUE INDEX contexto_nombre_uindex ON public.contexto USING btree (nombre);


--
-- Name: rol_permiso_rol_id_permiso_id_uindex; Type: INDEX; Schema: public; Owner: leviatan_dba
--

CREATE UNIQUE INDEX rol_permiso_rol_id_permiso_id_uindex ON public.rol_permiso USING btree (rol_id, permiso_id);


--
-- Name: usuario_rol_id_index; Type: INDEX; Schema: public; Owner: leviatan_dba
--

CREATE INDEX usuario_rol_id_index ON public.usuario_rol USING btree (usuario_id, rol_id);


--
-- Name: permiso permiso_contexto_contexto_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.permiso
    ADD CONSTRAINT permiso_contexto_contexto_id_fk FOREIGN KEY (contexto_id) REFERENCES public.contexto(contexto_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: permiso_hijo permiso_hijo_hijo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.permiso_hijo
    ADD CONSTRAINT permiso_hijo_hijo_id_fkey FOREIGN KEY (hijo_id) REFERENCES public.permiso(permiso_id);


--
-- Name: permiso_hijo permiso_hijo_padre_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.permiso_hijo
    ADD CONSTRAINT permiso_hijo_padre_id_fkey FOREIGN KEY (padre_id) REFERENCES public.permiso(permiso_id);


--
-- Name: permiso permiso_padre_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.permiso
    ADD CONSTRAINT permiso_padre_id_fkey FOREIGN KEY (padre_id) REFERENCES public.permiso(permiso_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: rol rol_contexto_contexto_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.rol
    ADD CONSTRAINT rol_contexto_contexto_id_fk FOREIGN KEY (contexto_id) REFERENCES public.contexto(contexto_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: rol_hijo rol_hijo_hijo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.rol_hijo
    ADD CONSTRAINT rol_hijo_hijo_id_fkey FOREIGN KEY (hijo_id) REFERENCES public.rol(rol_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rol_hijo rol_hijo_padre_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.rol_hijo
    ADD CONSTRAINT rol_hijo_padre_id_fkey FOREIGN KEY (padre_id) REFERENCES public.rol(rol_id);


--
-- Name: rol rol_padre_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.rol
    ADD CONSTRAINT rol_padre_id_fkey FOREIGN KEY (padre_id) REFERENCES public.rol(rol_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: rol_permiso rol_permiso_permiso_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.rol_permiso
    ADD CONSTRAINT rol_permiso_permiso_id_fkey FOREIGN KEY (permiso_id) REFERENCES public.permiso(permiso_id);


--
-- Name: rol_permiso rol_permiso_rol_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.rol_permiso
    ADD CONSTRAINT rol_permiso_rol_id_fkey FOREIGN KEY (rol_id) REFERENCES public.rol(rol_id);


--
-- Name: usuario_rol usuario_rol_rol_rol_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT usuario_rol_rol_rol_id_fk FOREIGN KEY (rol_id) REFERENCES public.rol(rol_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: usuario_rol usuario_rol_usuario_usuario_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: leviatan_dba
--

ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT usuario_rol_usuario_usuario_id_fk FOREIGN KEY (usuario_id) REFERENCES public.usuario(usuario_id) ON UPDATE CASCADE ON DELETE CASCADE;
--
-- Incializar datos del sistema raiz leviatan
--

INSERT INTO public.contexto (contexto_id, nombre, configuracion)
    VALUES ('dbbfc8be-0b0d-11eb-b260-0242ac130002', 'leviatan','{}');

INSERT INTO public.rol (rol_id, nombre, configuracion, contexto_id)
    VALUES ('bc026e86-0b0e-11eb-a975-0242ac130002', 'root', '{}','dbbfc8be-0b0d-11eb-b260-0242ac130002');

INSERT INTO public.permiso (permiso_id, nombre, configuracion, contexto_id)
    VALUES ('406e7c3c-0b0f-11eb-b391-0242ac130002','root', '{}','dbbfc8be-0b0d-11eb-b260-0242ac130002');

INSERT INTO public.usuario (usuario_id, info)
    VALUES ('c346c2b8-0b0f-11eb-9218-0242ac130002','{"password":"$6$JEq3iorhZ/mAv.vD$Ynh/RB3Eram5UsRqTufW/ZJR71iXQARfMDh3f7s5tVF.TZSdqGMm3SNF9uV3EmBNgvH87uVCoLhbhQhJ4.Ulw."}');

INSERT INTO public.rol_permiso (rol_permiso_id, rol_id, permiso_id)
    VALUES ('bc026e86-0b0e-11eb-a975-0242ac130002'||'406e7c3c-0b0f-11eb-b391-0242ac130002', 'bc026e86-0b0e-11eb-a975-0242ac130002', '406e7c3c-0b0f-11eb-b391-0242ac130002');

INSERT INTO public.usuario_rol (usuario_rol_id, usuario_id, rol_id)
    VALUES ('c346c2b8-0b0f-11eb-9218-0242ac130002'||'bc026e86-0b0e-11eb-a975-0242ac130002', 'c346c2b8-0b0f-11eb-9218-0242ac130002', 'bc026e86-0b0e-11eb-a975-0242ac130002');

--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

\connect postgres

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0 (Debian 13.0-1.pgdg100+1)
-- Dumped by pg_dump version 13.0 (Debian 13.0-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
