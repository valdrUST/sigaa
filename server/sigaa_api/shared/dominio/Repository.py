#!/usr/bin/env python

class Repository():
    """
    Clase de repositorio de datos
    """
    def __init__(self):
        self.list = []

    def insert(self, element):
        if element not in self.list:
            self.list.append(element)
            return True
        return False
    
    def get_list(self):
        return self.list

    def set_list(self, new_list):
        self.list = new_list
