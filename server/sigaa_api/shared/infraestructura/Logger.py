#!/usr/bin/env python
import logging

class Logger():
    """
    Clase para la adiministracion de logs
    """
    def __init__(self, filename="sigaa_api.log"):
        self.logger = logging.getLogger(__name__)

        c_handler = logging.StreamHandler()
        f_handler = logging.FileHandler(filename)
        c_handler.setLevel(logging.WARNING)
        f_handler.setLevel(logging.ERROR)

        # Create formatters and add it to handlers
        c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
        f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        c_handler.setFormatter(c_format)
        f_handler.setFormatter(f_format)

        self.logger.addHandler(c_handler)
        self.logger.addHandler(f_handler)
    
    def get_logger(self):
        return self.logger
    @staticmethod
    def set_logger():
        """
        Obtener un logger
        """
        logging.basicConfig(level=logging.DEBUG)
        log = Logger()
        return log.get_logger()

logger = Logger.set_logger()