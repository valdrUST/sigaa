#!/usr/bin/env python

class Arbol(object):
    """
    Clase Arbol
    """
    def __init__(self, id):
        self.id = id
        self.padre = None
        self.hijos = []
    
    def get_id(self):
        """
        getter id
        """
        return self.id
    
    def set_id(self, id):
        self.id = id

    def add_hijo(self, hijo):
        if self.is_padre(hijo):
            return None
        hijo.padre = self
        self.hijos.append(hijo)
        return hijo
    
    def get_hijos(self):
        return self.hijos

    def get_hijo_by_id(self, id):
        for hijo in self.hijos:
            if hijo.get_id() == id:
                return hijo
        return None
    
    def remove_hijos(self):
        huerfanos = []
        for hijo in self.hijos:
            huerfanos.append(self.remove_hijo_by_id(hijo.get_id()))
        return huerfanos


    def remove_hijo_by_id(self, id):
        hijo = self.get_hijo_by_id()
        if hijo != None:
            self.hijos.remove(hijo)
            hijo.padre = None
            return hijo
        return None
    
    def get_padre(self):
        return self.padre

    def is_padre(self, nodo):
        padre = self.get_padre()
        if padre != None:
            if padre.is_padre(nodo):
                return True
        if self.get_id() == nodo.get_id():
            return True
        return False


if __name__ == "__main__":
    nodo1 = Arbol("1")
    nodo2 = Arbol("2")
    nodo3 = Arbol("3")

    nodo4 = Arbol("4")
    nodoTest2 = Arbol("1")
    nodo3 = nodo2.add_hijo(nodo3)
    nodo2 = nodo1.add_hijo(nodo2)
    print(nodo3.is_padre(nodo4))

    print(nodo1.hijos)
    print(nodo2)
    print(nodo3)
