import json

from sigaa_api.shared.infraestructura.Database import Database
from sigaa_api.rbac.aplicacion.RbacAPP import RbacAPP
from sigaa_api.shared.infraestructura.Logger import logger

class RbacPGSQL(RbacAPP):
    """
    Clase para la persistencia en postgreSQL
    """
    def __init__(self):
        super().__init__()
        self.db = Database()
    
    def crear_perfil(self):
        a = super().crear_perfil()
        cursor = self.db.cursor()
        cursor.execute("""
            INSERT INTO usuario (usuario_id, info) VALUES (%s,%s)
        """,(str(a.get_id()),json.dumps(a.get_info()),))
        self.db.conn.commit()
        return a

    def crear_rol(self, nombre):
        r = super().crear_rol(nombre)
        cursor = self.db.cursor()
        cursor.execute("""
            INSERT INTO rol (rol_id, nombre, configuracion)
                VALUES (%s, %s, %s)
        """,(str(r.get_id()), r.get_nombre(), json.dumps(r.get_configuracion())))
        self.db.conn.commit()
        return r
    
    def crear_permiso(self, nombre):
        p = super().crear_permiso(nombre)
        cursor = self.db.cursor()
        cursor.execute("""
            INSERT INTO permiso (permiso_id, nombre, configuracion)
                VALUES (%s, %s, %s)
        """,(str(p.get_id()), p.get_nombre(), json.dumps(p.get_configuracion())))
        self.db.conn.commit()
        return p
    
    def add_perfil(self, perfil_json:str):
        """
        Añadir un perfil a la base de datos
        """
        a = self.deserialize_json_autenticacion(perfil_json)
        cursor = self.db.cursor()
        cursor.execute("""
            INSERT INTO usuario (usuario_id, info) VALUES (%s,%s)
        """,(str(a.get_id()),json.dumps(a.get_info()),))
        self.db.conn.commit()
        return a
    
    def add_rol(self, rol_json:str):
        """
        Añadir un rol a la base de datos
        """
        r = self.deserialize_json_rol(rol_json)
        cursor = self.db.cursor()
        cursor.execute("""
            INSERT INTO rol (rol_id, nombre, configuracion)
                VALUES (%s, %s, %s)
        """,(r.get_id(), r.get_nombre(), json.dumps(r.get_configuracion())))
        self.db.conn.commit()
        return r
    
    def add_permiso(self, permiso_json:str):
        """
        Añadir un permiso a la base de datos
        """
        p = self.deserialize_json_permiso(permiso_json)
        cursor = self.db.cursor()
        cursor.execute("""
            INSERT INTO permiso (permiso_id, nombre, configuracion)
                VALUES (%s, %s, %s)
        """,(p.get_id(), p.get_nombre(), json.dumps(p.get_configuracion())))
        self.db.conn.commit()
        return p
    
    def update_perfil(self, perfil_json:str):
        a = self.deserialize_json_autenticacion(perfil_json)
        cursor = self.db.cursor()
        cursor.execute("""
            UPDATE usuario SET info=%s
                WHERE usuario_id=%s
        """,(json.dumps(a.get_info()), a.get_id()))
        self.db.conn.commit()
        return a
    
    def update_rol(self, rol_json:str):
        r = self.deserialize_json_rol(rol_json)
        cursor = self.db.cursor()
        cursor.execute("""
            UPDATE rol SET nombre=%s, configuracion=%s
                WHERE rol_id=%s
        """,(r.get_nombre(),json.dumps(r.get_configuracion()), r.get_id()))
        self.db.conn.commit()
        return r
    
    def update_permiso(self, permiso_json:str):
        p = self.deserialize_json_permiso(permiso_json)
        cursor = self.db.cursor()
        cursor.execute("""
            UPDATE permiso SET nombre=%s, configuracion=%s
                WHERE permiso_id=%s
        """,(p.get_nombre(), json.dumps(p.get_configuracion()), p.get_id()))
        self.db.conn.commit()
        return p
            
    def delete_perfil(self, usuario_id:str):
        self.get_perfil(usuario_id)
        cursor = self.db.cursor()
        cursor.execute("""
            DELETE FROM usuario WHERE usuario_id=%s
        """,(usuario_id,))
        self.db.conn.commit()
        return True
    
    def delete_rol(self, rol_id:str):
        cursor = self.db.cursor()
        cursor.execute("""
            DELETE FROM rol WHERE rol_id=%s
        """,(rol_id,))
        self.db.conn.commit()
        self.db.conn.close()
        return True
    
    def delete_permiso(self, permiso_id:str):
        cursor = self.db.cursor()
        cursor.execute("""
            DELETE FROM permiso WHERE permiso_id=%s
        """,(permiso_id,))
        self.db.conn.commit()
        return True

    def add_perfil_rol(self, usuario_id, rol_id):
        if self.get_perfil(usuario_id) != None and self.get_rol(rol_id) != None:
            cursor = self.db.cursor()
            cursor.execute("""
                INSERT INTO usuario_rol(usuario_rol_id, usuario_id, rol_id)
                    VALUES (%s, %s, %s)
            """,((usuario_id+rol_id),usuario_id,rol_id))
            self.db.conn.commit()
            return True
        else:
            raise Exception("Rol o Perfil no existe en la base de datos")
            return False
    
    def del_perfil_rol(self, usuario_id, rol_id):
        cursor = self.db.cursor()
        cursor.execute("""
            DELETE FROM usuario_rol WHERE usuario_rol_id = %s
        """,((usuario_id+rol_id),))
        self.db.conn.commit()
        return True
    
    def add_rol_permiso(self, rol_id, permiso_id):
        if self.get_permiso(permiso_id) != None and self.get_rol(rol_id) != None:
            cursor = self.db.cursor()
            cursor.execute("""
                INSERT INTO rol_permiso (rol_id, permiso_id, rol_permiso_id)
                    VALUES (%s, %s, %s)
            """,(rol_id, permiso_id, (rol_id+permiso_id)))
            self.db.conn.commit()
            return True
        else:
            raise Exception("Rol o permiso no existe en la base de datos")
    
    def del_rol_permiso(self, rol_id, permiso_id):
        cursor = self.db.cursor()
        cursor.execute("""
            DELETE FROM rol_permiso where rol_permiso_id = %s
        """,(rol_id+permiso_id))
        self.db.conn.commit()
        return True
    
    def add_rol_hijo(self, rol_id, hijo_id):
        cursor = self.db.cursor()
        cursor.execute("""
            INSERT INTO rol_hijo(rol_hijo_id, padre_id, hijo_id)
                VALUES (%s, %s, %s)
        """,((rol_id+hijo_id),rol_id,hijo_id))
        cursor.execute("""
            UPDATE rol
            SET padre_id=%s
            WHERE rol_id = %s
        """,(rol_id, hijo_id))
        self.db.conn.commit()
        return True
    
    def del_rol_hijo(self, rol_id, hijo_id):
        cursor = self.db.cursor()
        cursor.execute("""
            DELETE FROM rol_hijo WHERE rol_hijo_id = %s
        """,((rol_id+hijo_id)))
        cursor.execute("""
           UPDATE rol
            SET padre_id = NULL
            WHERE rol_id = %s
            """,(hijo_id))
        self.db.conn.commit()
        return True
    
    def add_permiso_hijo(self, permiso_id, hijo_id):
        cursor = self.db.cursor()
        cursor.execute("""
            INSERT INTO permiso_hijo(permiso_hijo_id,padre_id, hijo_id)
                VALUES (%s, %s, %s)
        """,((permiso_id+hijo_id), permiso_id,hijo_id))
        cursor.execute("""
            UPDATE permiso
            SET padre_id = %s
            WHERE permiso_id = %s
        """,(permiso_id, hijo_id))
        self.db.conn.commit()
        return True
    
    def del_permiso_hijo(self, permiso_id, hijo_id):
        cursor = self.db.cursor()
        cursor.execute("""
            DELETE FROM permiso_hijo WHERE permiso_hijo_id = %s
        """,((permiso_id+hijo_id)))
        cursor.execute("""
           UPDATE permiso
            SET padre_id = NULL
            WHERE permiso_id = %s
            """,(hijo_id))
        self.db.conn.commit()
        return True
    
    def get_permiso(self, permiso_id, has_hijo=False):
        cursor = self.db.cursor()
        cursor.prepare("""
            SELECT
                json_build_object(
                'id',p.permiso_id,
                'nombre', p.nombre,
                'configuracion',p.configuracion,
                'contexto_id',p.contexto_id,
                'padre',p.padre_id,
                'hijos_id',array_agg(h.permiso_id)) permiso
            FROM permiso p
                LEFT JOIN permiso_hijo ph
                    ON p.permiso_id = ph.padre_id
                LEFT JOIN permiso h
                    ON ph.hijo_id = h.permiso_id
                WHERE p.permiso_id = %s
            GROUP BY p.permiso_id, p.nombre, p.padre_id
        """)
        cursor.execute((permiso_id,))
        res = cursor.fetch(fetch="one", to_dict=True)
        permiso_dict = res["permiso"]
        permiso_dict["hijos"] = []
        for hijo in permiso_dict["hijos_id"]:
            if hijo != None:
                permiso_dict["hijos"].append(self.get_permiso(hijo,has_hijo=True))
        if has_hijo:
            return permiso_dict
        permiso = self.deserialize_json_permiso(json.dumps(permiso_dict))
        return permiso
    
    def get_permisos(self):
        permisos = []
        cursor = self.db.cursor()
        cursor.execute("""
            SELECT
                permiso_id
            FROM permiso
        """)
        permisos_dict = cursor.fetch(fetch="all", to_dict=True)
        for permiso_dict in permisos_dict:
            permisos.append(self.get_permiso(permiso_dict["permiso_id"]))
        return permisos

    def get_rol(self, rol_id, has_hijo=False):
        cursor = self.db.cursor()
        cursor.prepare("""
            SELECT
                json_build_object(
                'id',r.rol_id,
                'nombre', r.nombre,
                'configuracion',r.configuracion,
                'contexto_id',r.contexto_id,
                'padre',r.padre_id,
                'permisos_id',array_agg(rp.permiso_id),
                'hijos_id',array_agg(h.rol_id)) rol
            FROM rol r
                LEFT JOIN rol_permiso rp
                    ON r.rol_id = rp.rol_id
                LEFT JOIN rol_hijo rh
                    ON r.rol_id = rh.padre_id
                LEFT JOIN rol h
                    ON rh.hijo_id = h.rol_id
            WHERE r.rol_id = %s
            GROUP BY r.rol_id, r.nombre, r.padre_id
        """)
        cursor.execute((rol_id,))
        res = cursor.fetch(fetch="one", to_dict=True)
        rol_dict = res["rol"]
        rol_dict["hijos"] = []
        for hijo in rol_dict["hijos_id"]:
            if hijo != None:
                rol_dict["hijos"].append(self.get_rol(hijo,has_hijo=True))
        if has_hijo:
            return rol_dict
        rol_dict["permisos"] = []
        rol = self.deserialize_json_rol(json.dumps(rol_dict))
        for permiso_id in rol_dict["permisos_id"]:
            if permiso_id != None:
                rol.add_permiso(self.get_permiso(permiso_id))
        return rol
        
    def get_roles(self):
        roles = []
        cursor = self.db.cursor()
        cursor.execute("""
            SELECT
                rol_id
            FROM rol
        """)
        roles_dict = cursor.fetch(fetch="all", to_dict=True)
        for rol_dict in roles_dict:
            roles.append(self.get_rol(rol_dict["rol_id"]))
        return roles
    
    def get_perfil(self, usuario_id):
        cursor = self.db.cursor()
        cursor.prepare("""
            SELECT
                json_build_object(
                    'id', u.usuario_id,
                    'info', u.info,
                    'roles_id', array_agg(ur.rol_id)
                ) usuario
            FROM usuario u
                LEFT JOIN usuario_rol ur
                    ON u.usuario_id = ur.usuario_id
            WHERE u.usuario_id = %s
            GROUP BY u.usuario_id
        """)
        cursor.execute((usuario_id,))
        res = cursor.fetch(fetch="one", to_dict=True)
        perfil_dict = res["usuario"]
        perfil_dict["roles"] = []
        perfil = self.deserialize_json_autenticacion(json.dumps(perfil_dict))
        for rol_id in perfil_dict["roles_id"]:
            if rol_id != None:
                perfil.add_rol(self.get_rol(rol_id))
        return perfil

    def get_perfiles(self):
        perfiles = []
        cursor = self.db.cursor()
        cursor.execute("""
            SELECT
                usuario_id
            FROM usuario
        """)
        perfiles_dict = cursor.fetch(fetch="all", to_dict=True)
        for perfil_dict in perfiles_dict:
            perfiles.append(self.get_perfil(perfil_dict["usuario_id"]))
        return perfiles
    
    def get_contexto(self, contexto_id:str):
        cursor = self.db.cursor()
        cursor.prepare("""
            SELECT json_build_object(
                'id',c.contexto_id,
                'nombre',c.nombre,
                'configuracion',c.configuracion,
                'roles_id', array_agg(r.rol_id),
                'permisos_id', array_agg(p.permiso_id)
                ) contexto
            FROM contexto c
                LEFT JOIN rol r
                    ON c.contexto_id = r.contexto_id
                LEFT JOIN permiso p
                    ON c.contexto_id = p.contexto_id
            WHERE c.contexto_id = %s
            GROUP BY c.contexto_id, c.nombre;
        """)
        cursor.execute((contexto_id,))
        res = cursor.fetch(fetch="one", to_dict=True)
        contexto_dict = res["contexto"]
        contexto_dict["roles"] = []
        contexto_dict["permisos"] = []
        contexto = self.deserialize_json_contexto(json.dumps(contexto_dict))
        for rol_id in contexto_dict["roles_id"]:
            if rol_id != None:
                contexto.set_rol(self.get_rol(rol_id))
        for permiso_id in contexto_dict["permisos_id"]:
            if permiso_id != None:
                contexto.set_permiso(self.get_permiso(permiso_id))
        return contexto
    
    def get_contextos(self):
        contextos = []
        cursor = self.db.cursor()
        cursor.execute("""
            SELECT
                contexto_id
            FROM contexto
        """)
        contextos_dict = cursor.fetch(fetch="all", to_dict=True)
        for contexto_dict in contextos_dict:
            contextos.append(self.get_contexto(contexto_dict["contexto_id"]))
        return contextos
    
    def set_contexto_rol(self, contexto_id, rol_id):
        cursor = self.db.cursor()
        contexto = self.get_contexto(contexto_id)
        rol = self.get_rol(rol_id)
        if super().set_contexto_rol(contexto, rol):
            hijos = rol.get_hijos()
            for hijo in hijos:
                cursor.execute("""
                    UPDATE rol
                        SET contexto_id = %s
                    WHERE
                        rol_id = %s
                """,(contexto_id,hijo.get_id(),))
                permisos = hijo.get_permisos()
                for permiso in permisos:
                    self.set_contexto_permiso(contexto_id, permiso.get_id())
            cursor.execute("""
                    UPDATE rol
                        SET contexto_id = %s
                    WHERE
                        rol_id = %s
            """,(contexto_id,rol_id,))
            self.db.conn.commit()
            return True
        return False
    
    def set_contexto_permiso(self, contexto_id, permiso_id):
        cursor = self.db.cursor()
        contexto = self.get_contexto(contexto_id)
        permiso = self.get_permiso(permiso_id)
        if super().set_contexto_permiso(contexto, permiso):
            hijos = permiso.get_hijos()
            for hijo in hijos:
                cursor.execute("""
                    UPDATE permiso
                        SET contexto_id = %s
                    WHERE
                        permiso_id = %s
                """,(contexto_id,hijo.get_id(),))
            cursor.execute("""
                UPDATE permiso
                    SET contexto_id = %s
                WHERE
                    permiso_id = %s
            """,(contexto_id,permiso_id,))
            self.db.conn.commit()
            return True
        return False
    
    def crear_contexto(self, nombre, configuracion = {}):
        cursor = self.db.cursor()
        contexto = super().crear_contexto(nombre)
        if contexto:
            try:
                cursor.prepare("""
                    INSERT INTO contexto (contexto_id, nombre, configuracion)
                        VALUES (%s, %s, %s)
                """)
                cursor.execute((contexto.get_id(),contexto.get_nombre(), json.dumps(configuracion),))
                self.db.conn.commit()
                return contexto
            except:
                return False
        return False
    
    def update_contexto(self, contexto_json):
        cursor = self.db.cursor()
        c = self.deserialize_json_contexto(contexto_json)
        cursor.execute("""
            UPDATE INTO contexto SET nombre = %s, configuracion = %s
                WHERE contexto_id = %s
        """,(c.get_id(),c.get_nombre(),c.get_configuracion()))
        self.db.conn.commit()
        return True


    def init_contexto(self):
        self.poblar_contexto_repo(json.dumps(self.get_contextos()))

if __name__ == "__main__":
    rbacSQL = RbacPGSQL()
    #res = json.dumps(rbacSQL.select_perfil("d4fe1b3e-0694-11eb-80e1-fcaa149a8530"))
    #rbacSQL.insert_perfil(res)
    #p = rbacSQL.create_permiso("permiso_2")
    #rbacSQL.add_hijo_permiso('8ee81274-06a0-11eb-89b8-fcaa149a8530','f0ae269c-06a0-11eb-b128-fcaa149a8530')
    print(rbacSQL.get_contextos())
    #rbacSQL.crear_perfil()
    #print(rbacSQL.serialize_autenticacion_repo_json())
    #print(rbacSQL.select_rol('0d2464c8-069f-11eb-86d3-fcaa149a8530'))
    #rbacSQL.insert_permiso("{}")
